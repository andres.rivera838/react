import React from 'react';
import './play-pause.css';
import Figura from '../../icons/components/figure'

function PlayPause(props) {
  return (
    <div className="PlayPause">
      {
        props.pause ?
          <button
            onClick={props.handleClick}
          >
            <Figura.Play size={25} color="white" />
          </button>
        :
        <button
          onClick={props.handleClick}
        >
          <Figura.Pause size={25} color="white" />
        </button>
      }
    </div>
  )
}

export default PlayPause

