import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './media.css';

// componente puro 

class Media extends PureComponent{

    state = {
        author: this.props.author
    }
   /*  constructor(props) {
        super(props)

        this.state = {
            author: props.author
        }

        //this.handleClick = this.handleClick.bind(this)
    } */

    handleClick = (event) => { 
       /*  console.log(event);
        console.log(this.state.author);

        this.setState({
            author: "felipe"
        }) */
    }

    render() {

        const styles = {
            container: {
                color: '#44546b',
                cursor: 'pointer',                
                width: 260,
                border: '1px solid red'
            }
        }        

        return (
            <div className="Media" onClick={this.props.handleClick}>
                <div>
                    <img 
                        src={this.props.cover}
                        alt=""
                        width={260}
                        height={160}
                    />
                    <h3> {this.state.author}</h3>
                    <p> {this.props.title}</p>
                </div>
            </div>           
        )
    }
}

Media.propTypes = {
    images: PropTypes.string,
    title: PropTypes.string.isRequired, 
    author: PropTypes.string,
    type: PropTypes.oneOf(['video', 'audio'])
}

export default Media;
