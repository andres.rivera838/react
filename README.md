##props
    https://reactjs.org/docs/typechecking-with-proptypes.html


##Eventos
    https://reactjs.org/docs/handling-events.html


## ciclo de vida de un componente 

    classMiComponenteextendsComponents{

        constructor(){
        // Enlazo (bind) eventos y/o inicializo estado
        }

        componentWillMount(){
        // Se ejecuta antes de montar el componente
        // Se podría usar para hacer un setState()
        }
        
        render(){
        // Contiene todos los elementos a renderizar
        // podrías usarlo para calcular propiedades (ej: concatenar una cadena)
        }

        componentDidMount(){
        //Solo se lanza una vez
        //Ideal para llamar a una API, hacer un setInteval, etc
        }

        //Actualización:
        componentWillReceiveProps(){
        //Es llamado cuando el componente recibe nuevas propiedades.
        }

        shouldComponentUpdate(){
        //Idea para poner una condición y  si las propiedades que le llegaron anteriormente
        // eran las mismas que tenia retornar false para evitar re-renderear el componente
        }

        componentWillUpdate(){
        //metodo llamado antes de re-renderizar el componente si shouldComponentUpdate devolvió true
        }W

        // re-render si es necesario...

        componentDidUpdate(){
        //Método llamado luego del re-render
        }

        componentWillUnmount(){
        //Método llamado antes de desmontar el componente
        }

        componentDidCatch(){
        // Si ocurre algún error, lo capturo desde acá:
        }

    }

#% Tipos de componentes

##PureComponent: 
    tiene el método shouldComponentUpdate ya asignado (por defecto), si a este componente no se le actualizan las propiedades, no tenemos que validar a mano con shouldComponentUpdate, PureComponent lo hace por nosotros, es decir; si recibe nuevas propiedades pero son las que ya teniamos, no se re-renderiza.

        importReact, { PureComponent } from 'react';

        classPlaylistextendsPureComponent{
        render() {
            <Componente />
            }
        }

##Componente Funcional: 
    Es una función la cual solo retorna el JSX de nuestro componente (renderiza UI), es mas sencillo, mas fácil de probar y este componente no tiene ciclo de vida.

    import React from'react';

    functionPlaylist(props) {
    return<Componentetitle={props.title} />
    }

#Portales 
    es la manera en la que podemos renderizar componentes fuera del contenedor principal de la aplicación.

    <divid="App"></div>

    El caso de uso mas común son las ventanas modal.

    Para crear un portal, se debe importar el metodo createPortal de react-dom:

    import { createPortal} from'react-dom';

    El metodo createPortal() recibe dos parametros, al igual que con render es Lo que se va a renderizar y donde se va a renderizar:

    classModalContainerextendsComponent{
    render() {
        return (
        createPortal(<Component />, document.getElementById('component-container'))
        )
    }
    }

